 

/**
 * Perulangan sebenarnya ada 3, yaitu for, while dan do while
 * Perbedaan ketiganya adalah di "cara perlakuan"
 * Maksudnya adalah, bagaimana suatu "block code" bakal dieksekusi -> 
 * 
 * jadi for itu sebenernya sama kayak if, dia akan mengeksekusi sesuai dengan block code nya
 * 
 * block kode kan selalu diawali dengan ({) dan diakhiri (})
 * 
 * Sebelum kita masuk ke contoh program, lebih baik kalau kita mempelajari struktur dari for
 * 
 * for(initialize;condition;increment/decrement){
 *          // disini kodenya
 * }
 * 
 * initialize = ini biasanya yang int i = 0; int f = 1; initialiasi sebuah variabel
 * yang hanya bisa dieksekusi didalam "badannya for", jadi selain di dalam for
 * variabel ini tidak akan bisa dibaca
 * 
 * condition = ini biasanya yang ditulis kayak i < 5, atau bisa juga i < n, atau bisa juga
 * i > 0, dan banyak bentuk dari kondisi ini, sesuaikan dengan logika yang akan digunakan
 * 
 * increment / decrement = semacam penambah ataupun pengurang dari suatu variabel initialize
 * increment atau decrement ada dua macam 
 * post dan pre, di solo learn kamu udah mempelajari ini
 * post increment / decrement merupakan suatu variabel yang di inisialisasi dipakai dulu baru ditambah / dikurang
 * pre increment / decrement merupakan suatu variabel yang di tambah / kurang dulu, baru dipakai 
 * 
 * contoh post increment / decrement
 * x++, x--, x = x + 2
 * pre increment / decrement
 * --x, ++x
 * 
 * jadi perulangan for akan "mengeksekusi program sesuai dengan kondisinya"
 * contoh
 * 
 * for(int i = 0; i < 5; i++)
 * 
 * diatas kalau dibaca kayak gini
 * ada i sama dengan 0, i tidak boleh lebih dari 5, i ditambahkan sebanyak 1 tingkat
 *
 * 
 * for(int i = 0; i < 3; i++){
 *      cout<<i<<endl;
 * }
 * 
 * kalau diurut akan kayak gini
 * i = 0; apakah 0 < 3 ?, jika ya keluarkan i sekarang, // 0, tambah 1 ke i
 * i = 1; apakah 1 < 3 ?, jika ya keluarkan i sekarang, // 1, tambah 1 ke i
 * i = 2; apakah 2 < 3 ?, jika ya keluarkan i sekarang, // 2, tambah 1 ke i
 * i = 3; apakah 3 < 3 ?, ternyata tidak, maka perulangan berhenti
 * 
 * jadi nanti outpunya adalah
 * 0 //enter 1 //enter 2
 * 
 * bagaimana jika i = i + 2 ?
 * sama saja sebenarnya, hanya saja tambah 2 ke i,
 * kalau i = i - 2 ?
 * sama saja, kurangi 2 ke i, disetiap perulangan
 * kalau i = i + 5 ? 
 * sama saja, tambah 5 ke i, disetiap perulangan
 **/