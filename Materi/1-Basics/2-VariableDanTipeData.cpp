/**
 * Bab 1.2
 * Variable dan tipe data
 **/

/**
 * IMPORTANT NOTE !!
 * C++ sangat sensitif sekali dengan huruf besar dan kecil dalam suatu identifiers
 * jadi harap diingat dalam penulisan identifiers
 **/

/**
 * Identifiers
 * Identifier merupakan semacam bentuk identifikasi terhadap suatu fungsi, variable maupun yang lainnya
 * Identifier harus
 * 1. Tidak boleh ada spasi
 * 2. Hanya huruf, digit, dan underscore/underline ( _ )
 * 3. Selalu diawali oleh huruf atau underscore, tapi dalam penggunaan underscore harus berbeda nama dengan
 * yang telah diambil oleh sistem
 **/

/**
 * Tipe Data
 * 1. signed/ unsigned char (signed (-128 to 127) / unsigned (0 to 255))
 * 2. signedint (-2147483648 to 2147483647) / unsigned int (0 to 4294967295)
 * 3. signed short int (-32768 to 32767) / unsigned short int (0 to 65535)
 * 4. signed long int (-2147483648 to 2147483647) / unsigned long int (0 to 4294967295)
 * 3. bool (true or false)
 * 4. float  (+/- 3.4e +/- 38 (~7 digits))
 * 5. double (+/- 1.7e +/- 308 (~15 digits))
 * 6. long double (+/- 1.7e +/- 308 (~15 digits))
 * 7. wchar_t
 * 8. string
 * 
 * secara default compiler akan mengambil signed untuk tipe data nomor 1 - 4
 **/

#include <iostream>
#include <string>
using namespace std;
/**
 * Deklarasi Variabel
 **/

int a;       // bisa dengan ini
int b, c, d; // atau juga dengan ini jika tipe datanya sama

// atau dengan yang dibawah ini, meskipun sebenarnya kalau ditulis bisa seperti yang diatas
float e;
float f;
float g;

// sebagai contoh deklarasi variabel

int main()
{
    short int angkaPendek = 32767;
    int angka = 2147483647;
    long int angkaPanjang = 4294967295;
    char huruf = 'c';
    bool boolean = false;
    float desimalFloat = 1.1;
    double desimalDouble = 1.1;
    long double desimalLongDouble = 1.2;
    string kalimat = "ini kalimat";
    return 0;
}
