/**
 * Bab 1.3
 * Konstanta
 **/

/**
 * Konstanta merupakan ekspresi untuk menetapkan nilai tetap dari suatu variabel
 * seperti yang telah dicontohkan pada bab 1.2
 **/

// definisi kan sebuah konstanta menggunakan define
// atau bisa disebut defined konstanta

#define PI 3.14

// dengan cara diatas kita telah membuat konstanta PI dengan nilai 3.14

// atau menggunakan cara

const int R = 3.14;