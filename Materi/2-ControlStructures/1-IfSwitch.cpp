/**
 * Bab 2.1
 * If dan Switch
 **/

/**
 * If digunakan untuk melakukan eksekusi jika kondisinya terpenuhi
 * 
 * if(kondisi) statement;
 * 
 * contoh
 *      if(x==10){
 *          cout<<"angkanya"<<x;
 *      }
 * Diatas merupakan contoh dimana akan menampilkan angka x, jika x benar 10
 * 
 * atau
 *      if(x==10){
 *          cout<<"angkanya"<<x;
 *      }else{
 *          cout<<"angkanya bukan 10";
 *      }
 * 
 * Jika x tidak bernilai 10 maka akan mengeksekusi block statement yang bawah
 * 
 * atau 
 *      if(x > 0){
 *          cout<<"positif";
 *      }else if(x < 0){
 *          cout<<"negatif";
 *      }else{
 *          cout<<"x nya 0";
 *      }
 * 
 * Jika x merupakan 10 maka akan mengeksekusi block statement yang pertama
 * Jika x merupakan -1 maka akan mengeksekusi block statement yang kedua
 * Jika x merupakan 0 maka akan mengeksekusi block statement yang ketiga
 * 
 * 
 **/

/**
 * Switch
 * 
 * Hampir sama dengan if tetapi dikhususkan untuk nilai pasti
 * Maksudnya adalah, kalau di if kondisinya sesuai dengan yang diinginkan
 * Tetapi di switch ini kondisinya harus nilai yang konstan
 * 
 * contoh
 *      switch(x){
 *          case 1:
 *              //block statement
 *              break;
 *          case 2:
 *              //block statement
 *              break;
 *          default:
 *              //block statement
 *              break;
 *      }
 * 
 * Dalam hal ini x harus merupakan kondisi yang menghasilkan nilai konstan
 **/