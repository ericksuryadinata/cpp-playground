/**
 * Bab 1.1
 * Pengenalan struktur di C++
 **/

// ini merupakan komentar, tapi hanya untuk satu line saja, biasanya disebut "line comment"

/** 
 * Ini juga komentar, yang digunakan
 * bisa lebih dari satu line
 * atau bisa juga disebut "block comment"
 **/

/**
 * Semua bentuk komentar tidak akan diikutkan pada saat proses compile
 **/

#include <iostream>

/**
 * diatas merupakan preprocessor, sebuah fungsi untuk memanggil semacam library standard untuk input output
 * kita dapat membuat sendiri preprocessor tersebut, akan dipelajari di bab berikutnya
 **/

using namespace std;

/**
 * diatas merupakan deklarasi untuk menggunakan namespace dengan nama std.
 * apa itu namespace? namespace bisa dikatakan sebagai group.
 * simplenya sih sebagai pembeda antara identifiers (tipe data, fungsi, variable, dll)
 **/

/**
 * Dibawah ini merupakan fungsi utama dari sebuah program C++, jadi semua hal dilakukan disini
 **/
int main()
{

     cout << "Hello World";
     /**
     * Diatas merupakan statement atau kode yang dituliskan di C++
     * cout berasal dari iostream yang telah kita includekan yang merupakan salah satu yang berada
     * dibawah namespace std
     **/
     return 0;
     /**
     * merupakan return statement, yang berguna untuk memberi tahu bahwa program kita udah selesai
     **/
}