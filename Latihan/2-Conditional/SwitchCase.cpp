#include <iostream>

using namespace std;

int main()
{
    int kelas;
    int nilai;

    cout<<"Masukkan kelas : ";
    cin>>kelas;
    cout<<"Masukkan Nilai : ";
    cin>>nilai;

    switch (kelas)
    {
        case 1:
            
            if(nilai >= 90){
                cout<<"A";
            }else if(nilai >= 80){
                cout<<"B";
            }else if(nilai >= 70){
                cout<<"C";
            }

            break;
        case 2:

            if(nilai >= 90){
                cout<<"D";
            }else if(nilai >= 80){
                cout<<"E";
            }else if(nilai >= 70){
                cout<<"F";
            }

            break;
        case 3:

            if(nilai >= 90){
                cout<<"G";
            }else if(nilai >= 80){
                cout<<"H";
            }else if(nilai >= 70){
                cout<<"I";
            }

            break;
        default:
            cout<<"Kelas tidak ditemukan";
            break;
    }
}