#include <iostream>
using namespace std;

int main()
{

    // simple for
    // anggep aku punya variabel ini
    /*int jumlah = 0; 
    for(int i = 0; i < 5; i++)
    {
        cout<<i<<endl;
        jumlah += i;
    } 
    cout<<"Jumlah nya = "<<jumlah;  */

    // advanced for
    /** int batas = 5;
    for(int i = 0; i < batas ; i++)
    {
        // jadi gini sayanggg
        // kalau dirunut gimana yakk?
        // i = 0, cek i < batas, keluarkan i dan koma ( 0, ) , i ditambah 1
        // i = 1, cek i < batas, keluarkan i dan koma ( 0,1, ) , i ditambah 1
        // keliatannya kita seharusnya tau polanya kan yaa
        // yang diperlukan berarti cek, jika i itu merupakan angka terakhir sebelum batas
        // karena kita butuh cek maka
        if(i == batas - 1){ 
            // kenapa kok batas - 1, kan sesuai kesepakatan, 
            // merupakan angka terakhir sebelum batas
            cout<<i;
        }else{
            cout<<i<<","; // run gih kok ribet yah ada if juga
            // ya iyaa lahh, kan harus ngecek dulu, ngecek kan pake if
            // diatas itu berarti, jika i belum sama dengan batas - 1,
            // maka keluarkan yang di else, paham kan ??sek tak pahami 
            // kalau di cek satu2
            // i , i < batas, i == batas - 1, output, increment
            // 0 , 0 < 5, 0 == 4, (0,) -> masih else, 1
            // 1 , 1 < 5, 1 == 4, (0,1,) -> masih else, 2
            // 2 , 2 < 5, 2 == 4, (0,1,2,) -> masih else, 3
            // 3 , 3 < 5, 3 == 4, (0,1,2,3,) -> masih else, 4
            // 4 , 4 < 5, 4 == 4, (0,1,2,3,4) -> nah ini masuk if, 5
            // 5 , 5 < 5, stop
        }
    }
**/
    /* int n,i; 
    int jumlah=0;
    int nilai[n];
   
    cout<<"Masukkan jumlah bilangan : "; 
    cin>>n;
    
    for(i=0;i<n;i++){ 
        cin>>nilai[i];
        jumlah +=nilai[i];
    }
    float mean = (float)jumlah/n;
    cout<<"Rata - ratanya adalah : "<<mean;
    return 0; */

    /*
    int angka;
    int jumlah;
    int max, min;
    
    cout << "Mencari nilai terbesar dan terkecil" << endl;
    cout << "masukkan jumlah angka : ";
    cin >> jumlah;
    
    for (int i = 1; i <= jumlah; i++) {
        cout << "Masukkan Angka : ";
        cin >> angka; // pertama kan nginput ini
    
        if ( i == 1 ) { // kalau masih angka yang pertama kali
            min = angka; // langsung set min dan max sesuai inputan pertama kali
            max = angka;
        }
        else if ( min > angka ) { // nah yang ini
            min = angka; // bakal ngecek inputan selanjutnya,
            // jika angka yang diinput lebih kecil, maka min nya akan berubah
        }
        else if ( max < angka) { // begitupun dengan ini
            max = angka; // bakal ngecek inputan selanjutnya
            // jika angka yang diinput lebih besar, maka max nya akan berubah
        }
    }

    // contoh jumlah angka = 5
    // inputan pertama 7, maka min = 7, max = 7
    // inputan kedua 10, maka cek min dulu, apakah min > angka, 7 > 10, maka min tetep, dan block code tidak dijalankan
    // masuk ke max < angka, 7 < 10, nahhh, jadi block code ini dijalnkan, max = 10
    // kondisi sekarang min = 7, max = 10
    // begitupun seterusnya sampe angka habis.

    cout << endl;
    cout << "Nilai terkecil     : " <<  min << endl;
    cout << "Nilai terbesar     : " <<  max << endl;

    return 0;
 */

    //mencetak karakter bintang (for dalam for)
    /*  int kolom;
    int baris;
    int max;
    char karakter;

    karakter ='*'; //deklarasikan dulu bentuk karaktetnya
    max = 10;        //maksimal barisnya
    cout<<"==Menampilkan karakter bintang=="<<endl;

    for(baris=1;baris<=max;baris++){//baris awal punya nilai 1; lalu kondisinya baris=1 ini kurang
                                    //dari max yaitu 5; lalu baris tadi ditambah 1

        for(kolom=1;kolom<=baris;kolom++){//nilai kolom awal juga 1; lalu kondisinya kolom lebih kecil
                                        //dari baris karena sama2 1 maka kolom juga ditambah 1.
                cout<<karakter; 
        }
        cout<<endl;
    }

    return 0;
*/

    //bintang bentuk segitiga

    /*int t;
	cout<<"Tinggi segitiga : "; cin>>t;
   for (int i=t; i>=1;i--)
     {
     if(i!=t)
        for (int j=i; j<t; j++)
            {cout<<' ';}

     for (int k=i; k>1; k--)
            {cout<<'*';}

     for (int l=1; l<=i; l++)
            {cout<<'*';}
   cout<<endl;}


   for (int i=2; i<=t;i++)
     {if(i!=t)
     for (int j=i; j<t; j++)
     {cout<<' ';}
     for (int k=i; k>1; k--)
     {cout<<'*';}
     for (int l=1; l<=i; l++)
     {cout<<'*';}
   cout<<endl;}

   return 0;
*/

    //menampilkan deret geometri 0,1,3,6. . .
/*
    int a, b, c, n;
    cout << "menampilkan deret bilangan 0,1,3,6..." << endl;
    cout << "masukkan banyaknya bilangan yang akan di tampilkan=";
    cin >> n;

    b = (0);                 //b awal dalam kondisi 0, // sebenarnya gak pake () ini aja boleh kok
    c = (-1);                //c awal -1 // ini juga
    for (a = 1; a <= n; a++) // berarti kan a dimulai dari 1 sampai a tidak lebih besar sama dengan n, dengan increment 1
    {
        c = c + 1;          // ketika a 1, maka c = 0. 0 + 1 = 1 (c jadi 1 karena udah diupdate c= -1+1 = 0, lalu 0+1 =1)
        b = b + c;         // ketika a 1, maka b = 0 + 1 = 1     (begitu juga dengan b awlnya 0 diupdate jadi b=0+1 =1)
        cout << "deret ke '" << a << "' = " << b << endl; // cout deret nya
    }
    // note :biasakan indentasi antara curly bracket {} sebagai penutup dibuat sejajar ya, biar gak bingung :)
    // how to read it
    // ------------------------------------------------
    // |  a  |        c      |      b      |  a <= n  |
    // |  -  |       -1      |      0      |  - <= 5  | -> ini masih ditahap awal, masih input n
    // |  1  |  (-1) + 1 = 0 |  0 + 0 = 0  |  1 <= 5  | -> c sebelumnya -1 ditambah 1 jadi 0, b sebelumnya 0 ditambah 0 jadi 0
    // |  2  |  (0) + 1 = 1  |  0 + 1 = 1  |  2 <= 5  | -> c sebelumnya 0 ditambah 1 jadi 1, b sebelumnya 0 ditambah c yang 1 tadi, b sekarang 1
    // |  3  |  (1) + 1 = 2  |  1 + 2 = 3  |  3 <= 5  | -> c sebelumnya 1 ditambah 1 jadi 2, b sebelumnya 1 ditambah c yang 2 tadi, b sekarang 3
    // |  4  |  (2) + 1 = 3  |  3 + 3 = 6  |  4 <= 5  | -> c sebelumnya 2 ditambah 1 jadi 3, b sebelumnya 3 ditambah c yang 3 tadi, b sekarang 6 
    // |  5  |  (3) + 1 = 4  |  6 + 4 = 10 |  5 <=5   | -> c sebelumnya 3 ditambah 1 jadi 4, b sebelumnya 6 ditambah c yang 4 tadi, b sekarang 10 
    // begitupun seterusnya
    return 0;
*/

    //menampilkan deret genap 0, 2, 4, 6 , , ,
    //menampilkan deret ganjil 1,3,5,7

    // int ganjil=1, genap=0;
    // int n;

    // cout<<"MENAMPILKAN DERET BILANGAN GENAP DAN GANJIL"<<endl;
    // cout<<"Masukkan bilangan yang akan ditampilkan : ";
    // cin>>n;
    // cout<<"Ganjil = ";
    // for(int a=1; a<=n; a++)
    // {
    //     if(a==n){
    //         cout<<ganjil;
    //     }
    //     else{
    //         cout<<ganjil<<",";
    //     }
    //     ganjil= ganjil+2;
        
    // }
    // cout<<endl;
    // cout<<"Genap = ";
    // for(int a=1; a<=n; a++)
    // { 
       
    //     if (a==n) {
    //         cout<<genap;
    //     }
    //     else{
    //         cout<<genap<<",";
    //     }
    //      genap= genap+2; 
    // }
    // return 0;

    int c,a=0 , b=1;
    int n;
    int fibo = 1;

    cout<<"DERET FIBONACCI"<<endl;
    cout<<"Deret ke-";
    cin>>n;

    for(int k=0; k<n; k++)
    {
        if(k==a){
            cout<<a<<",";
        }else if(k==b){
            cout<<b<<",";
        }else{
            c = fibo;
            fibo = fibo + a;
            a = c; 
            
            if(k==n-1)
            {
                cout<<fibo;

            }else
            {
                cout<<fibo<<",";
            }
        }
    }

    return 0;

}